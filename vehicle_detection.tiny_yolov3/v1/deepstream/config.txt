[property]
gpu-id=0
#net-scale-factor=1
net-scale-factor=0.0039215697906911373
#0=RGB, 1=BGR
model-color-format=0
custom-network-config=/home/awiros-docker/alexandria/vehicle_detection.tiny_yolov3/v1/darknet/yolov3_vcc.cfg
model-file=/home/awiros-docker/alexandria/vehicle_detection.tiny_yolov3/v1/darknet/yolov3_vcc.weights
#model-engine-file=/home/awiros-docker/model_b1_gpu0_fp16.engine
#model-engine-file=/home/awiros-docker/cadmus.engine
#labelfile-path=yolov3-tiny.names
labelfile-path=/home/awiros-docker/alexandria/vehicle_detection.tiny_yolov3/v1/darknet/obj.names
## 0=FP32, 1=INT8, 2=FP16 mode
network-mode=2
num-detected-classes=7
gie-unique-id=1
is-classifier=0
# try with cluster mode 3 as well
cluster-mode=2
maintain-aspect-ratio=0
parse-bbox-func-name=NvDsInferParseYolo
#custom-lib-path=nvdsinfer_custom_impl_Yolo/libnvdsinfer_custom_impl_Yolo.so
custom-lib-path=/opt/nvidia/deepstream/deepstream/lib/libnvdsinfer_custom_impl_Yolo.so
engine-create-func-name=NvDsInferYoloCudaEngineGet



[class-attrs-all]
nms-iou-threshold=0.3
pre-cluster-threshold=0.410000
