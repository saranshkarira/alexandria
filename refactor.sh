#!/usr/bin/env bash

alex_dir=/home/rajateor_appstore/alexandria
allowedExts=()
dvcExts=("onnx" "weights" "wts") #"pt" "pth" 
gitExts=("cfg" "names" "data" "txt" "dvc" "json" "refactor.sh")

allowedExts+=(${dvcExts[@]})
allowedExts+=(${gitExts[@]})

Todelete=()
dvcFiles=()
gitFiles=()


IFS=$'\n'
files=($(find . -maxdepth 6 -type f -not -path '*/\.*' | sed 's/^\.\///g' | sort)) # $(PWD) for absolute path
unset IFS

len=${#files[*]}
echo -e " \n"
printf "There are %s files in alexandria right now!! \n\n" "${len}"

#Handling files to delete
for file in "${files[@]}"
do
	short=($(sed -n 's/^\(.*\/\)*\(.*\)/\2/p' <<< $file))
	arrIN=(${short//./ })
	# echo ${arrIN[1]

	#Adding files to delete
	if [[ ! " ${allowedExts[*]} " =~ " ${arrIN[1]} " ]]; then
		if [[ ! " ${allowedExts[*]} " =~ " ${short} " ]]; then
			Todelete+=("$file")
		fi
	fi
done

printf "%s\n" "${Todelete[@]}" > .todelete
sed -i "1s/^/Deleting following files: (Remove path to not delete)\n/" .todelete
sed -i "2s/^/\n/" .todelete

< /dev/tty vim -o .todelete
sed -i -e '1d;2d' .todelete
sed -i  "/^ *$/d" .todelete
# perl -i -lne 'print if(/./)' .todelete

readarray -t Todelete < .todelete


len=${#Todelete[*]}
printf "You have deleted %s files!! \n\n" "${len}"

for file in "${Todelete[@]}"
do
		rm $file
done

rm .todelete

# printf "%s\n" "${files[@]}"

#Refactoring alexandria


IFS=$'\n'
files=($(find . -maxdepth 6 -type f -not -path '*/\.*' | sed 's/^\.\///g' | sort)) # $(PWD) for absolute path
refactorFiles=($(printf -- '%s\n' "${files[@]}" | grep -E "onnx/[vV][0-9]+|deepstream/[vV][0-9]+|darknet/[vV][0-9]+")) # use numbers to remove any false detects like onnx/vehicle
unset IFS
array=("onnx" "deepstream" "darknet")

for oldfile in "${refactorFiles[@]}"
do
	# echo $oldfile
	arrIN=(${oldfile//// })
	# pos=$("grep -m1 -Fxn [vV][0-9] <<<"${arrIN[@]}" | cut -d: -f1")
	# IFS=$'\n'
	# pos=$(printf "%s\n" "${arrIN[@]}" | grep -Em1 -n "onnx|deepstream|darknet" | cut -d: -f1)
	pos=$(printf "%s\n" "${arrIN[@]}" | grep -Em1 -n "onnx|deepstream|darknet" | cut -d: -f1)
	# echo $pos

	temp=${arrIN[$pos-1]}
	arrIN[$pos-1]=${arrIN[$pos]}
	arrIN[$pos]=${temp}
	newfile=$(IFS=/ ; echo "${arrIN[*]}")
	# echo $newfile
	mkdir -p `dirname ${newfile}`
	mv "$oldfile" "$newfile"
	
done

# moving single darknet and deepstream to versions with onnx


# IFS=$'\n'
# halfFiles=($(find . -maxdepth 4 -type f -not -path '*/\.*' | sed 's/^\.\///g' | grep -E "deepstream|darknet" )) # $(PWD) for absolute path
# unset IFS



# printf "%s\n" "${halfFiles[@]}"



#Moving specs in same directory as versions

IFS=$'\n'
files=($(find . -maxdepth 6 -type f -not -path '*/\.*' | sed 's/^\.\///g' | sort)) # $(PWD) for absolute path
specFiles=($(printf -- '%s\n' "${files[@]}" | grep -E "[vV][0-9]+/onnx/.*json$"))
unset IFS

# printf "%s\n" "${specFiles[@]}"

for oldspec in "${specFiles[@]}"
do
	# echo $oldspec
	arrIN=(${oldspec//// })
	pos=$(printf "%s\n" "${arrIN[@]}" | grep -Em1 -n "onnx" | cut -d: -f1)
	unset arrIN[$pos-1]
	newspec=$(IFS=/ ; echo "${arrIN[*]}")
	# echo $newspec
	mv "$oldspec" "$newspec"
	
done


Todelete=($(find . -maxdepth 5 -type d -empty -print -not -path '*/\.*' ))
Todelete+=($(find . -maxdepth 5 -type f -name ".*" ))

printf "%s\n" "${Todelete[@]}" > .todelete
sed -i "1s/^/Deleting following files and empty folders: (Remove path to not delete)\n/" .todelete
sed -i "2s/^/\n/" .todelete

< /dev/tty vim -o .todelete
sed -i -e '1d;2d' .todelete
sed -i  "/^ *$/d" .todelete
# perl -i -lne 'print if(/./)' .todelete

readarray -t Todelete < .todelete

len=${#Todelete[*]}

# find . -type d -depth -print0 > delete.txt

for file in "${Todelete[@]}"
	do
		rm -rf $file
		find `dirname $file` -maxdepth 5 -type d -empty -delete
		# echo $file
done

rm .todelete
printf "You have deleted %s empty folders!! \n\n" "${len}"

#Handling files for git and dvc
#
IFS=$'\n'
files=($(find . -maxdepth 6 -type f -not -path '*/\.*' | sed 's/^\.\///g' | sort)) # $(PWD) for absolute path
unset IFS


# Getting files with version and .onnx or .wts or .weights extension to add to dvc


mapfile -t tempgitFiles < <(git ls-files -o)

#tracked dvc files
tracked_dvc_Files=($(find . -type f -maxdepth 6 | grep -E "[/][vV][0-9][/].*[a-z0-9A-Z].(\.onnx|.*\.wts|.*.weights).*.dvc$"))

#untracked weight files. Adding hard condition to only add files in dvc with exact correct way of hierarchy (v0-9/onnx|darknet|deepstream/weight_file)
#
weightFiles=($(find . -type f -maxdepth 6 | grep -E "[/][vV][0-9][/].*(onnx|darknet|deepstream).*[a-z0-9A-Z].(\.onnx|.*\.wts|.*.weights)$"))
# weightFiles=($(find . -type f -maxdepth 6 | grep -E "[/][vV][0-9][/].*(onnx|darknet|deepstream).*[a-z0-9A-Z].*\.${dvcExts[*]}"))

printf "%s\n" "${weightFiles[@]}" > .fordvc

sed -i "1s/^/Adding following files to dvc: (Remove files from here to do otherwise)\n/" .fordvc
sed -i "2s/^/\n/" .fordvc

< /dev/tty vim -o .fordvc

sed -i -e '1d;2d' .fordvc
sed -i  "/^ *$/d" .fordvc
# perl -i -lne 'print if(/./)' .fordvc

readarray -t weightFiles < .fordvc

len=${#weightFiles[*]}
echo -e " \n"
printf "Adding % files!! \n\n" "${len}"

printf "%s\0" "${weightFiles[@]}" | xargs -0 dvc add --

# dvc commit

# git ls-files -o > .forgit
# sleep  4

# mapfile -t tempgitFiles < <(git ls-files -o | grep -E "${gitExts[@]}$")
IFS="|"
mapfile -t tempgitFiles < <(find . -type f -maxdepth 6 -not -path '*/\.*' | grep -E "${gitExts[*]}$")
unset IFS

# for file in "${tempgitFiles[@]}"
# do
#   short=($(sed -n 's/^\(.*\/\)*\(.*\)/\2/p' <<< $file))
#   arrIN=(${short//./ })

#   # echo "${arrIN[1]}"
#   #Add git files to add, push
#   if [[ " ${gitExts[*]} " =~ " ${arrIN[1]} " ]]; then
#       gitFiles+=("$file")
#   fi

# done

printf "%s\n" "${tempgitFiles[@]}" > .forgit

mapfile -t tempgitFiles < <(git ls-files -m)

printf "%s\n" "${tempgitFiles[@]}" >> .forgit

sed -i "1s/^/Adding following files to git: (Remove files from here to do otherwise)\n/" .forgit
sed -i "2s/^/\n/" .forgit

< /dev/tty vim -o .forgit

sed -i -e '1d;2d' .forgit
sed -i  "/^ *$/d" .forgit
#perl -i -lne 'print if(/./)' .forgit

readarray -t tempgitFiles < .forgit

len=${#tempgitFiles[*]}
echo -e " \n"
printf "Adding % files!! \n\n" "${len}"

printf "%s\0" "${tempgitFiles[@]}" | xargs -0 git add --

# commit message
> .gitcommit
echo -n "Please add a commit message for all the changes:" >> .gitcommit

sed -i -e '$a\' .gitcommit


< /dev/tty vim -o .gitcommit

sed -i -e '1d;2d' .gitcommit
sed -i  "/^ *$/d" .gitcommit

# readarray -t tempgitmessage < .forgit
tempgitmessage=$(<.gitcommit)

git commit -m "$tempgitmessage"

